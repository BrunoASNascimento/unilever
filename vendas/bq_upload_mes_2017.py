import requests
from urllib.request import urlopen
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
import pandas as pd
import pandas_gbq
import os
import shutil

# caminho_serv = '/var/scripts/inmet/'
caminho_windows = input('Digite o caminho: ').replace('\\', '/')+'/'


def push_bq(data_frame):
    df = data_frame
    projectid = 'pluvion-tech'
    name_table = 'pluvion_customers.UNILEVER_VENDAS_V2'
    pandas_gbq.to_gbq(
        df, name_table,
        project_id=projectid,
        if_exists='append'
    )
    print('Upload: '+str(df.shape))


def list_file(caminho):
    list_file = os.listdir(caminho)
    return list_file


def move_file(caminho, file):
    caminhoCompleto_old = caminho+file
    caminhoCompleto_new = caminho+'backup/'+file
    shutil.move(caminhoCompleto_old, caminhoCompleto_new)


def clean_data(list_file, caminho):
    ano = str(2017)
    for file in list_file:
        if file.endswith(".xlsx"):
            print(file)
            mes =['JAN',
                       'FEB',
                       'MAR',
                       'APR',
                       'MAY',
                       'JUN',
                       'JUL',
                       'AUG',
                       'SEP',
                       'OCT',
                       'NOV',
                       'DEC']
            for p in range(1,13):
                columns = ['ForecastUnit',
                        'SubDivision',
                        'Segment',
                        'Customer',
                        'ANO',
                        'MES',
                        'Venda']
                df = pd.read_excel(caminho+file,   skiprows=[
                    i for i in range(0, 10)])
                df['ForecastUnit'] = df['Forecast Unit']
                df['SubDivision'] = df['Sub Division']
                df['ANO'] = ano
                df['MES'] = p
                df['Venda'] = df[mes[p-1]+'-'+ano]
                

                df_ed = pd.DataFrame(df, columns=columns)

                # df = pd.read_excel()
                print(df_ed)
                print(mes[p-1]+'-'+ano)
                df_ed.to_csv(caminho + 'csv/' + file.replace('xlsx', '') +str(ano)+str(p).zfill(2)+ '.csv', index=False)
                push_bq(df_ed)
                # move_file(caminho, file)


clean_data(list_file(caminho_windows), caminho_windows)
