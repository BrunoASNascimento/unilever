import requests
from urllib.request import urlopen
from bs4 import BeautifulSoup
from datetime import datetime, timedelta
import pandas as pd
import pandas_gbq
import os
import shutil

# caminho_serv = '/var/scripts/inmet/'
caminho_windows = input('Digite o caminho: ').replace('\\', '/')+'/'


def push_bq(data_frame):
    df = data_frame
    projectid = 'pluvion-tech'
    name_table = 'pluvion_customers.UNILEVER_VENDAS'
    pandas_gbq.to_gbq(
        df, name_table,
        project_id=projectid,
        if_exists='append'
    )
    print('Upload: '+str(df.shape))


def list_file(caminho):
    list_file = os.listdir(caminho)
    return list_file


def move_file(caminho, file):
    caminhoCompleto_old = caminho+file
    caminhoCompleto_new = caminho+'backup/'+file
    shutil.move(caminhoCompleto_old, caminhoCompleto_new)


def clean_data(list_file, caminho):
    ano = str(2018)
    for file in list_file:
        if file.endswith(".xlsx"):
            print(file)
            columns = ['ForecastUnit',
                       'SubDivision',
                       'Segment',
                       'Customer',
                       'ANO',
                       'JAN',
                       'FEB',
                       'MAR',
                       'APR',
                       'MAY',
                       'JUN',
                       'JUL',
                       'AUG',
                       'SEP',
                       'OCT',
                       'NOV',
                       'DEC']
            df = pd.read_excel(caminho+file,   skiprows=[
                i for i in range(0, 10)])
            df['ForecastUnit'] = df['Forecast Unit']
            df['SubDivision'] = df['Sub Division']
            df['ANO'] = ano
            df['JAN'] = df['JAN-'+ano]
            df['FEB'] = df['FEB-'+ano]
            df['MAR'] = df['MAR-'+ano]
            df['APR'] = df['APR-'+ano]
            df['MAY'] = df['MAY-'+ano]
            df['JUN'] = df['JUN-'+ano]
            df['JUL'] = df['JUL-'+ano]
            df['AUG'] = df['AUG-'+ano]
            df['SEP'] = df['SEP-'+ano]
            df['OCT'] = df['OCT-'+ano]
            df['NOV'] = df['NOV-'+ano]
            df['DEC'] = df['DEC-'+ano]

            df_ed = pd.DataFrame(df, columns=columns)

            # df = pd.read_excel()
            print(df_ed)

            df_ed.to_csv(caminho + 'csv/' + file + '.csv', index=False)
            push_bq(df_ed)
            # move_file(caminho, file)


clean_data(list_file(caminho_windows), caminho_windows)
