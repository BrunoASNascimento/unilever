import pandas as pd
import os
from geopy.geocoders import ArcGIS
from geopy.extra.rate_limiter import RateLimiter
import pandas_gbq
import time


caminho_serv = '/var/scripts/unilever/pdv/data/'
#caminho_windows = input('Digite o caminho: ').replace('\\', '/')+'/'
#caminho_windows = 'G:/My Drive/Unilever/01. Dados originais/02. Cadastro PDV/'


def push_bq(data_frame):
    df = data_frame
    projectid = 'pluvion-tech'
    name_table = 'pluvion_customers.UNILEVER_PDV'
    pandas_gbq.to_gbq(
        df, name_table,
        project_id=projectid,
        if_exists='append'
    )
    print('Upload: '+str(df.shape))


def list_file(caminho):
    list_file = os.listdir(caminho)
    return list_file


def clean_data(list_file, caminho):
    #print(caminho, list_file)
    df_edit = pd.DataFrame()
    geolocator = ArcGIS()
    geocode = RateLimiter(geolocator.geocode, min_delay_seconds=0)
    for file_name in list_file:
        if file_name.endswith(".xlsx"):
            file_name = caminho+file_name
            print(file_name)
            sheet_list = pd.ExcelFile(file_name)
            sheet_name = sheet_list.sheet_names
            for sheet in sheet_name:
                print(sheet)
                df = pd.read_excel(file_name, sheet_name=sheet)
                print('Tamanho: ', df.shape)
                df['cep'] = df['Código pos']
                df['SoldTo'] = df['Sold to']
                df['Organizacao'] = df['Organizaçã']
                df['Rua'] = df['Rua e nº']
                df['Regiao'] = df['Região']
                df['CentroFor'] = df['Centro for']
                df['SubDivisao'] = df['Sub-Divisã']
                df['DescrSub'] = df['Descr Sub-']
                df['CanalMerc'] = df['Canal Merc']
                df['DescrCana'] = df['Descr Cana']
                df['DescrSegmento'] = df['Descr Segmento']
                step = 10
                for i in range(0, len(df['cep']), step):

                    df_edit = df.iloc[(i):(i+step), :]
                    df_edit['location'] = df_edit['cep'].apply(geocode)
                    df_edit['point'] = df_edit['location'].apply(
                        lambda loc: tuple(loc.point) if loc else None)
                    df_edit['lat'] = df_edit['point'].apply(
                        lambda loc: loc[0] if loc else None)
                    df_edit['lon'] = df_edit['point'].apply(
                        lambda loc: loc[1] if loc else None)

                    df_edit = df_edit[
                        [
                            'SoldTo',
                            'Organizacao',
                            'Nome',
                            'Rua',
                            'Cidade',
                            'Regiao',
                            'cep',
                            'CentroFor',
                            'SubDivisao',
                            'DescrSub',
                            'CanalMerc',
                            'DescrCana',
                            'Segmento',
                            'DescrSegmento',
                            'location',
                            'lat',
                            'lon'
                        ]
                    ]

                    print(df_edit.shape, str(i))
                    df_edit.to_csv('./backup/pdv_unilever_'+str(i).zfill(6) +
                                   '_'+str(i+10).zfill(6)+'.csv', index=False)
                    time.sleep(15)
                    push_bq(df_edit)
            # print(df)


clean_data(list_file(caminho_serv), caminho_serv)
