import pandas_gbq

projectid = 'pluvion-tech'
name_table = 'pluvion_customers.UNILEVER_PDV'

df = pandas_gbq.read_gbq(
    'SELECT distinct * FROM `pluvion_customers.UNILEVER_PDV`',
    project_id=projectid)

print(df)
df.to_csv('./pdv_unilever_inmet.csv', index=False)
